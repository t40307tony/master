package com.example.hw_alarmclock1;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class ShowAlarmActivity extends AppCompatActivity {

    TextView mTime1;
    TextView mTime2;
    TextView mTime3;
    CheckBox box1, box2, box3;
    private Intent service;
    private ClockService clockService;
    private ServiceConnection[] clockConnection;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_alarm);

        mTime1 = (TextView) findViewById(R.id.time1);
        mTime2 = (TextView) findViewById(R.id.time2);
        mTime3 = (TextView) findViewById(R.id.time3);
        box1 = (CheckBox) findViewById(R.id.box1);
        box2 = (CheckBox) findViewById(R.id.box2);
        box3 = (CheckBox) findViewById(R.id.box3);

        mTime1.setOnClickListener(this::onClick);
        mTime2.setOnClickListener(this::onClick);
        mTime3.setOnClickListener(this::onClick);

        box1.setOnClickListener(this::onClick2);
        box2.setOnClickListener(this::onClick2);
        box3.setOnClickListener(this::onClick2);

        clockConnection = new ServiceConnection[3];
        service = new Intent(this, ClockService.class);
        startService(service);
    }

    public void onClick2(View v) {
        TextView nowTime;
        CheckBox box = (CheckBox) v;
        int index;
        if(v.getId() == box1.getId()) {
            index = 0;
            nowTime = mTime1;
        }
        else if(v.getId() == box2.getId()) {
            index = 1;
            nowTime = mTime2;
        }
        else {
            index = 2;
            nowTime = mTime3;
        }
        if(box.isChecked()) {
            clockConnection[index] = new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                    clockService = ((ClockService.MyBinder) iBinder).getService();
                    Intent intent = new Intent();

                    Calendar calendar = Calendar.getInstance();

                    String timeString = nowTime.getText().toString().substring(0,5);
                    timeString = calendar.get(Calendar.YEAR)+"/"+(calendar.get(Calendar.MONTH)+1)+"/"+calendar.get(Calendar.DATE)+" "+ timeString;
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
                    Date setDate = null;
                    try {
                        setDate = sdf.parse(timeString);
                    } catch (ParseException e) {
                        setDate = new Date();
                        e.printStackTrace();
                    }
                    long time = setDate.getTime()-(new Date()).getTime();
                    if(time<0)
                        time+=24*60*60*1000;
                    intent.putExtra("MS", time);
                    intent.putExtra("ID", v.getId());
                    clockService.setClock(intent);
                }

                @Override
                public void onServiceDisconnected(ComponentName componentName) {
                }
            };
            service = new Intent(this, ClockService.class);
            bindService(service, clockConnection[index], Service.BIND_AUTO_CREATE);
        }
        else{
            if(clockService != null)
                clockService.stopClock(v.getId());
            unbindService(clockConnection[index]);
        }

    }

    public void onClick(View v) {
        Intent intent = new Intent();
        intent.putExtra("viewId", v.getId());
        intent.setClass(ShowAlarmActivity.this, SetAlarmActivity.class);
        startActivityForResult(intent, 1);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        int viewId = data.getIntExtra("viewId", -1);
        String timeMsg = data.getStringExtra(SetAlarmActivity.ALL_TIME);
        switch (viewId) {
            case R.id.time1:
                mTime1.setText(timeMsg);
                break;
            case R.id.time2:
                mTime2.setText(timeMsg);
                break;
            case R.id.time3:
                mTime3.setText(timeMsg);
                break;
            default:
                mTime1.setText(timeMsg);
                break;
        }
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "activity stoped.", Toast.LENGTH_SHORT).show();
        super.onDestroy();
        stopService(service);
    }
}
