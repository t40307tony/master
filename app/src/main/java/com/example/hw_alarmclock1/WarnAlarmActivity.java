package com.example.hw_alarmclock1;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class WarnAlarmActivity extends AppCompatActivity {

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_warn_alarm);

        TextView mShowAlarmTime = (TextView) findViewById(R.id.showAlarmTime);
        Button closeButton = (Button) findViewById(R.id.closeButton);
        Button laterButton = (Button) findViewById(R.id.laterButton);

        closeButton.setBackgroundColor(getColor(R.color.darkred));
        laterButton.setBackgroundColor(getColor(R.color.darkred));

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        laterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //過10分鐘再響一次
            }
        });


    }

}