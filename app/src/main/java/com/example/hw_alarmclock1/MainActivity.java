package com.example.hw_alarmclock1;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private TextView mTimeDisplay;
    private TextView mDateDisplay;
    private Button mButton;

    private int mMonth;
    private int mDay;
    private String mWeek;
    private int mHour;
    private int mMinute;
    private int mAmPm;
    private String castAmPm;

    String[] en = {"SUN", "MON", "TUE",
            "WED", "THU", "FRI", "SAT"};

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTimeDisplay = (TextView) findViewById(R.id.showTime);
        mDateDisplay = (TextView) findViewById(R.id.showDate);
        mButton = (Button) findViewById(R.id.button);
        mButton.setBackgroundColor(getColor(R.color.darkred));

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this,ShowAlarmActivity.class);
                startActivity(intent);
            }
        });

        Thread clockThread = new Thread(){
            @Override
            public void run(){
                while(true) {
                    getTime();
                    updateDisplay();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        clockThread.start();
    }

    private void getTime(){
        Calendar c = Calendar.getInstance();
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        //WEEK_OF_MONTH 一個月中第幾週
//        mWeek = en[c.get(Calendar.WEEK_OF_MONTH)];
        //DAY_OF_WEEK 星期幾
        mWeek = en[c.get(Calendar.DAY_OF_WEEK)-1];
        mHour = c.get(Calendar.HOUR);
        mMinute = c.get(Calendar.MINUTE);
        mAmPm = c.get(Calendar.AM_PM);
        if(mAmPm == 0){
            castAmPm = "AM";
        }else{
            castAmPm = "PM";
        }
    }

    private void updateDisplay(){
        mTimeDisplay.setText(new StringBuilder().append(pad(mHour)).append(":")
                .append((pad(mMinute))).append(" ").append(castAmPm));

        mDateDisplay.setText(new StringBuilder().append(mWeek)
                .append(", ").append(mMonth+1).append("/").append(mDay));
    }

    private String pad(int c){
        if(c >= 10)
            return String.valueOf(c);
        else
            return "0" + c;
    }
}