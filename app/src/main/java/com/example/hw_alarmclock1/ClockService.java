package com.example.hw_alarmclock1;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import androidx.annotation.Nullable;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ClockService extends Service {

    private Map<Integer, Thread> threadMap;
    private Map<Integer, Boolean> clockOpenMap;

    @Override
    public void onCreate(){
        threadMap = new ConcurrentHashMap<>();
        clockOpenMap = new ConcurrentHashMap<>();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
//        this.stopSelf();
        return new MyBinder();
    }

    public void setClock(Intent intent){
        long second = intent.getLongExtra("MS", 0L);
        int id = intent.getIntExtra("ID", -1);
        Thread clockthread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(second);
                } catch (
                        InterruptedException e) {
                    e.printStackTrace();
                }
                if(!clockOpenMap.get(id))
                    return;
                Intent newIntent = new Intent("com.example.hw.CLOCK");
                newIntent.setPackage("com.example.hw_alarmclock1");
                sendBroadcast(newIntent);

                threadMap.remove(id);
                clockOpenMap.remove(id);
            }
        };
        threadMap.put(id, clockthread);
        clockOpenMap.put(id, true);
        clockthread.start();
    }

    public void stopClock(int id){
        if(threadMap.get(id) != null) {
            threadMap.get(id).interrupt();
            clockOpenMap.put(id, false);
        }
    }



    public class MyBinder extends Binder {
        public ClockService getService() {
            return ClockService.this;
        }
    }
}
