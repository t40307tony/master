package com.example.hw_alarmclock1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class SetAlarmActivity extends AppCompatActivity {

    public static final String ALL_TIME = "";
    StringBuilder allTime = new StringBuilder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_alarm);

        Intent intent = getIntent();

        EditText setHour = (EditText) findViewById(R.id.leftText);
        EditText setMin = (EditText) findViewById(R.id.rightText);
        RadioGroup rg = (RadioGroup) findViewById(R.id.radioGroup);
        Button btn = (Button) findViewById(R.id.button);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(setHour.getText().toString().equals("") || setHour.getText() == null) {
                    Toast.makeText(SetAlarmActivity.this, "please input an value of hours.", Toast.LENGTH_SHORT).show();
                    return;
                }
                else if(setMin.getText().toString().equals("") || setMin.getText() == null) {
                    Toast.makeText(SetAlarmActivity.this, "please input an value of minutes.", Toast.LENGTH_SHORT).show();
                    return;
                }
                else if(rg.getCheckedRadioButtonId()==-1) {
                    Toast.makeText(SetAlarmActivity.this, "please select AM or PM.", Toast.LENGTH_SHORT).show();
                    return;
                }

                switch (rg.getCheckedRadioButtonId()){
                    case R.id.amButton:
                        allTime.append(setHour.getText()).append(":").append(setMin.getText())
                                .append(" ").append("AM");
                        break;
                    case R.id.pmButton:
                        allTime.append(setHour.getText()).append(":").append(setMin.getText())
                                .append(" ").append("PM");
                        break;
                }
                intent.putExtra(ALL_TIME, allTime.toString());
                setResult(1, intent);
                finish();
            }
        });

    }

    //限制EditText中只能輸入規定範圍的數字，並將小於10的數字補0
}